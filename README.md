# Oh my Food
Ce répertoire a pour but de présenter le projet n°3 pour la soutenance intitulé :
Dynamisez une page web avec des animations CSS.

----------------
# Démo
[Démo](https://jrosefab.gitlab.io/josephrosefabrice_3_06052021/)

----------------
# Technologies utilisés
Ce projet a été codé a partir des outils suivants :

* HTML
* SCSS / CSS (implémentation BEM)
* W3C Validator : https://validator.w3.org/

Ce projet est compatible sur tout type de navigateurs et est adapté aux format mobile/tablette/desktop.


# Installation
Pour lire projet, cloner celui ci puis double cliquer sur index.html afin de visualiser la page.
Pour toute modifications, il vous faudra compiler le scss. 
Si vous utiliser visual Studio, le plugin live-sass-compiler est fortement recommendé.

-----------------

# Livrable
Page d’accueil
Pages de menus

-----------------

Animations :
* Loader sur la page d'accueil
* Remplissage progressif du coeur
* Apparition horizontal en fading de chaque menu
* Apparition progressif du bouton de validation du menu


Polices :
* Logo et titres : Shrikhand
* Texte : Roboto

Couleurs :
* Primaire: #9356DC
* Secondaire: #FF79DA
* Tertiaire: #99E2D0
